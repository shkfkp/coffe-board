# coffe-board

This is a simple coffee-board application, that stores a list of coffees in the database,
serves it via restful web api, and presents it using react frontend.

## Prerequisites

- .NET 7 SDK (or newer)
- nodeJS v.16+
- PostgreSQL
- (optional) docker installation
- (optional) IDE or editor of your choice: VS, VS Code, Rider, Webstorm, etc.

## How to run locally

Clone git repository to your local machine.

### docker-compose (recommended)

If you have docker installed on local machine, it is recommended to use provided `docker-compose.yml` file,
which will create a database container, back-end container and front-end container. Using any Linux console emulator,
or CMD/Powershell on Windows: 
```docker compose up .```
Navigate to ```localhost:11080``` to interact with the application using your web browser.

### manual

Using either terminal emulator, or IDE/Editor of your choice, open `src/frontend` and `src/backend/Coffee.Api` folders. For
front-end application, execute `npm start` command, for backend-application, execute `dotnet run` command. If you are using 
Visual Studio or Rider, you can start application using `http` profile.  

## Project structure

### Back-end
Single-project solution, that contains the DbContext, Migrations, Controllers and related source files. This structure was picked bacause of overall
simplicity of given assignment. Scaffolded as `webapi` template of `dotnet new` command.

### Front-end
Simplistic and opinionated React + Typescript application in combination with webpack. No templates (i.e. react-scripts or similar) was used for scaffolding.
Micro-state management is achieved via react-query (which is using Hooks under the hood), routing - via react-router, styling - via TailwindCSS to speed up 
the development. Since no strict requirements applied to appearance of the application, a very basic styles were applied.

## Rationale

### Image storage
Images should be stored in the appropriate storage provider i.e. Azure Blob or AWS S3. For simplicity, in this assignment
images are stored in the database and cached on each read operation in order to reduce amount of DB queries. 

### Repository and Service abstractions
Given assignment does not involve any business logic related to database entries, controller methods use ICoffeeRepository
directly without an intermediate ICoffeeService layer. 
IImageService is a thin wrapper over libwebp to reduce disk space used by images (and also to reduce network traffic).

### Authentication and Authorization
This requirement is not mentioned in the assignment, but the simplest approach would be to utilize JWT access and refresh tokens, 
```[Authorize]``` attributes, and a ```User``` database table to persist user data.

