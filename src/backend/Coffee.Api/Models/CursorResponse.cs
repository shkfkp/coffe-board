﻿using System.Text.Json.Serialization;

namespace Coffee.Api.Models;

public class CursorResponse<T>
{
	[JsonPropertyName("content")] public T[] Content { get; set; }
	[JsonPropertyName("hasNext")] public bool HasNext { get; set; }
	[JsonPropertyName("hasPrev")] public bool HasPrev { get; set; }
	[JsonPropertyName("page")] public int Page { get; set; }

	public CursorResponse(T[] content, bool hasNext, bool hasPrev, int page)
	{
		Content = content;
		HasNext = hasNext;
		HasPrev = hasPrev;
		Page = page;
	}
}