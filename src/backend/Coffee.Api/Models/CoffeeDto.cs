﻿using System.Text.Json.Serialization;

namespace Coffee.Api.Models;

public class CoffeeDto
{
	[JsonPropertyName("id")] public Guid Id { get; set; }
	[JsonPropertyName("title")] public string Title { get; set; }
	[JsonPropertyName("price")] public decimal Price { get; set; }
}