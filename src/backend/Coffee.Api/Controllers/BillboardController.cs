﻿using Coffee.Api.Database.Repository;
using Coffee.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Coffee.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BillboardController : ControllerBase
{
	private readonly IImageService _imageService;
	private readonly IResetService _resetService;
	private readonly ICoffeeRepository _coffeeRepository;

	public BillboardController(
		IImageService imageService, 
		IResetService resetService,
		ICoffeeRepository coffeeRepository 
		)
	{
		_imageService = imageService;
		_resetService = resetService;
		_coffeeRepository = coffeeRepository;
	}
	
	[HttpGet("cursor")]
	public async Task<IActionResult> Cursor([FromQuery] int cursor)
	{
		if (cursor < 1) return BadRequest("Invalid cursor.");
		var response = await _coffeeRepository.GetCoffees(cursor);
		return Ok(response);
	}

	[HttpPost("add")]
	public async Task<IActionResult> Add(
		[FromForm(Name = "image")] IFormFile image,
		[FromForm(Name = "title")] string title,
		[FromForm(Name = "price")] decimal price
	)
	{
		if (image == null || image.Length == 0) return BadRequest("Image file is missing");
		if (string.IsNullOrWhiteSpace(title)) return BadRequest("Title is missing");
		if (price < 1.0m) return BadRequest("Price is invalid");

		var encodedImage = await _imageService.WebpEncode(image.OpenReadStream());
		if (encodedImage == null || encodedImage.Length == 0) return new UnsupportedMediaTypeResult();

		var id = await _coffeeRepository.AddCoffee(title, price, encodedImage);
		return Ok(id);
	}

	[HttpDelete("remove/{id:guid}")]
	public async Task<IActionResult> Remove([FromRoute] Guid id)
	{
		var result = await _coffeeRepository.RemoveCoffee(id);
		return Ok(result);
	}

	[HttpPost("reset")]
	public async Task<IActionResult> Reset()
	{
		await _resetService.ResetDatabase();
		return Ok();
	}
}