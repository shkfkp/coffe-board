﻿using Coffee.Api.Database.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace Coffee.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AssetsController : ControllerBase
{
	private readonly IDistributedCache _cache;
	private readonly ICoffeeRepository _coffeeRepository;

	public AssetsController(IDistributedCache cache, ICoffeeRepository coffeeRepository)
	{
		_cache = cache;
		_coffeeRepository = coffeeRepository;
	}

	[ResponseCache(Duration = 3600)]
	[HttpGet("image/{id:guid}")]
	public async Task<IActionResult> GetImage(Guid id)
	{
		var image = await _cache.GetAsync(id.ToString());
		if (image is not null && image.Length != 0) return File(image, "image/webp");
		var persistedImage = await _coffeeRepository.GetCoffeeImage(id);
		await _cache.SetAsync(id.ToString(), persistedImage);
		return File(persistedImage, "image/webp", id.ToString());
	}
}