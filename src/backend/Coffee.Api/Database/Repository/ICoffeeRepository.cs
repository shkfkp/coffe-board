using Coffee.Api.Models;

namespace Coffee.Api.Database.Repository;

public interface ICoffeeRepository
{
	public Task<CursorResponse<CoffeeDto>> GetCoffees(int page);
	public Task<byte[]> GetCoffeeImage(Guid id);
	public Task<Coffee> GetCoffee(Guid id);
	public Task<Guid> AddCoffee(string title, decimal price, byte[] image);
	public Task<bool> RemoveCoffee(Guid id);
}