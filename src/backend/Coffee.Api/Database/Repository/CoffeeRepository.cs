﻿using Coffee.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Coffee.Api.Database.Repository;

public class CoffeeRepository : ICoffeeRepository
{
	private readonly AppDbContext _context;

	public CoffeeRepository(AppDbContext context)
	{
		_context = context;
	}

	public async Task<CursorResponse<CoffeeDto>> GetCoffees(int page)
	{
		var count = await _context.Coffees.CountAsync();
		
		
		var hasPrevious = page > 1;
		var hasNext = page < (int)Math.Ceiling((double)count / 10);
		
		var coffees = await _context.Coffees
			.Select(x => new CoffeeDto
			{
				Id = x.Id,
				Title = x.Title,
				Price = x.Price
			})
			.OrderBy(x => x.Price)
			.Skip((page - 1) * 10)
			.Take(10)
			.ToArrayAsync();
		
		return new CursorResponse<CoffeeDto>(coffees, hasNext, hasPrevious, page);
	}

	public async Task<byte[]> GetCoffeeImage(Guid id)
	{
		var coffeeImage = await _context.Coffees
			.Where(x => x.Id == id)
			.Select(x => x.Image)
			.SingleOrDefaultAsync();
		return coffeeImage;
	}

	public async Task<Coffee> GetCoffee(Guid id)
	{
		var coffee = await _context.Coffees.FindAsync(id);
		return coffee;
	}

	public async Task<Guid> AddCoffee(string title, decimal price, byte[] image)
	{
		var count = await _context.Coffees.CountAsync();
		if (count > 50) return Guid.Empty;

		var coffee = new Coffee
		{
			Id = Guid.NewGuid(),
			CreatedAt = DateTimeOffset.UtcNow,
			Currency = "EUR",
			Price = price,
			Title = title,
			Image = image
		};
		_context.Coffees.Add(coffee);
		await _context.SaveChangesAsync();
		return coffee.Id;
	}

	public async Task<bool> RemoveCoffee(Guid id)
	{
		var coffee = await _context.Coffees.FindAsync(id);
		if (coffee is null) return false;
		_context.Coffees.Remove(coffee);
		await _context.SaveChangesAsync();
		return true;

	}
}