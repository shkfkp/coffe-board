﻿using Microsoft.EntityFrameworkCore;

namespace Coffee.Api.Database;

public class AppDbContext : DbContext
{
	public DbSet<Coffee> Coffees { get; set; }
	
	public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
	{
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.Entity<Coffee>().ToTable("coffees");

		modelBuilder.Entity<Coffee>(coffeeBuilder =>
		{
			coffeeBuilder.HasKey(c => c.Id);
			
			coffeeBuilder
				.Property(c => c.Id)
				.HasColumnName("id");
			
			
			
			
		});
	}
}