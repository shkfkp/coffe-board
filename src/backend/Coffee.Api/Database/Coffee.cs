﻿namespace Coffee.Api.Database;

public class Coffee
{
	public Guid Id { get; set; }
	public DateTimeOffset CreatedAt { get; set; }
	public string Title { get; set; }
	public string Currency { get; set; }
	public decimal Price { get; set; }
	public byte[] Image { get; set; }
}