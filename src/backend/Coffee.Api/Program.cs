using Coffee.Api.Database;
using Coffee.Api.Database.Repository;
using Coffee.Api.Services;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace Coffee.Api;

public class Program
{
	public static void Main(string[] args)
	{
		var builder = WebApplication.CreateBuilder(args);

		var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
		var dataSourceBuilder = new NpgsqlDataSourceBuilder(connectionString);
		var dataSource = dataSourceBuilder.Build();
		
		builder.Services.AddControllers();
		builder.Services.AddEndpointsApiExplorer();
		builder.Services.AddSwaggerGen();
		builder.Services.AddDbContext<AppDbContext>(options =>
		{
			options.UseNpgsql(dataSource);
		});
		builder.Services.AddDistributedMemoryCache();
		builder.Services.AddScoped<IImageService, ImageService>();
		builder.Services.AddScoped<IResetService, ResetService>();
		builder.Services.AddScoped<ICoffeeRepository, CoffeeRepository>();
		builder.Services.AddHostedService<SetupService>();
		
		builder.Services.AddCors(options =>
		{
			options.AddPolicy(name: "all",
				policy  =>
				{
					policy.AllowAnyOrigin();
					policy.AllowAnyHeader();
					policy.AllowAnyMethod();
				});
		});
		
		var app = builder.Build();
		if (app.Environment.IsDevelopment())
		{
			app.UseSwagger();
			app.UseSwaggerUI();
		}
		// app.UseHttpsRedirection();
		app.UseAuthorization();
		app.MapControllers();
		app.UseCors("all");
		app.Run();
	}
}