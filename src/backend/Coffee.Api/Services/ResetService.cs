﻿using Coffee.Api.Database;
using Microsoft.EntityFrameworkCore;

namespace Coffee.Api.Services;

public interface IResetService
{
	Task ResetDatabase();
}

public class ResetService : IResetService
{
	private readonly AppDbContext _context;

	public ResetService(AppDbContext context)
	{
		_context = context;
	}

	public async Task ResetDatabase()
	{
		string[] brands = ["Starbucks", "Costa", "Nescafe", "Lavazza", "Illy", "Folgers", "Maxwell House", "Peet's Coffee", "Dunkin' Donuts", "Tim Hortons"];
		string[] types = ["Espresso", "Cappuccino", "Latte", "Americano", "Macchiato", "Mocha", "Flat White", "Affogato", "Irish Coffee", "Turkish Coffee"];
		string[] images = ["1.webp", "2.webp", "3.webp", "4.webp"];
		
		var random = new Random(3072024);
		var coffees = new List<Database.Coffee>();
		for (var i = 0; i < 30; i++)
		{
			var rndBrand = brands[random.Next(0, brands.Length)];
			var rndType = types[random.Next(0, types.Length)];
			var rndImage = images[random.Next(0, images.Length)];

			var coffee = new Database.Coffee
			{
				Id = Guid.NewGuid(),
				CreatedAt = DateTimeOffset.UtcNow,
				Currency = "EUR",
				Price = (i + 1) * 10,
				Image = await File.ReadAllBytesAsync(Path.Combine("Assets", "coffees", rndImage)),
				Title = $"{rndBrand} {rndType}"
			};
			coffees.Add(coffee);
		}

		await _context.Coffees.Where(x => x.Id != Guid.Empty).ExecuteDeleteAsync();
		_context.Coffees.AddRange(coffees);
		await _context.SaveChangesAsync();
	}
}