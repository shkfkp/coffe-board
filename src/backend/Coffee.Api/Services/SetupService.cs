﻿using Coffee.Api.Database;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;

namespace Coffee.Api.Services;

public class SetupService : IHostedService
{
	private readonly ILogger<SetupService> _logger;
	private readonly IConfiguration _configuration;
	private readonly IServiceProvider _serviceProvider;

	public SetupService(ILogger<SetupService> logger, IConfiguration configuration, IServiceProvider serviceProvider)
	{
		_logger = logger;
		_configuration = configuration;
		_serviceProvider = serviceProvider;
	}

	public async Task StartAsync(CancellationToken cancellationToken)
	{
		var applyMigration = _configuration.GetValue<bool>("Setup:MigrateDatabase");
		var applySeed = _configuration.GetValue<bool>("Setup:SeedDatabase");
		var applyPermissions = _configuration.GetValue<bool>("Setup:ApplyPermissions");

		if (applyMigration)
		{
			await MigrateDatabaseAsync();
		}

		if (applySeed)
		{
			await SeedDatabaseAsync();
		}

		if (applyPermissions)
		{
			SetUnixFilePermissions();
		}
	}

	public Task StopAsync(CancellationToken cancellationToken)
	{
		return Task.CompletedTask;
	}

	private void SetUnixFilePermissions()
	{
		if (!RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) return;
		_logger.LogInformation("OS Platform requires setting +rx permission to library binaries");

		var basePath = AppDomain.CurrentDomain.BaseDirectory;
		var libsPath = Path.Combine(basePath, "Assets");

		var directory = new DirectoryInfo(libsPath);
		if (!directory.Exists) _logger.LogError("Assets directory was not found");

		var binariesDirectories = directory.GetDirectories("binaries", SearchOption.AllDirectories);
		foreach (var binariesDirectory in binariesDirectories)
		{
			var files = binariesDirectory.GetFiles();
			foreach (var file in files)
			{
				_logger.LogInformation("Setting +rx permission to file {Path}", file.FullName);
				File.SetUnixFileMode(file.FullName, UnixFileMode.UserRead | UnixFileMode.UserExecute);
			}
		}
	}

	private async Task SeedDatabaseAsync()
	{
		using var scope = _serviceProvider.CreateScope();
		var resetService = scope.ServiceProvider.GetRequiredService<IResetService>();
		await resetService.ResetDatabase();
	}
	
	private async Task MigrateDatabaseAsync()
	{
		using var scope = _serviceProvider.CreateScope();
		var context = scope.ServiceProvider.GetRequiredService<AppDbContext>();
		await context.Database.MigrateAsync();
	}
}