﻿using CliWrap;
using System.Runtime.InteropServices;
using System.Text;

namespace Coffee.Api.Services;

public interface IImageService
{
	Task<byte[]> WebpEncode(Stream input);
}

public class ImageService : IImageService
{
	private readonly Architecture _architecture;
	private readonly string _os;

	public ImageService()
	{
		if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) _os = "osx";
		else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) _os = "linux";
		else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) _os = "windows";
		else _os = "unknown";

		_architecture = RuntimeInformation.ProcessArchitecture;
	}

	public async Task<byte[]> WebpEncode(Stream input)
	{
		var encodeCommand = GetEncodeCommand();
		if (!Path.Exists(encodeCommand))
			throw new FileNotFoundException("Cwebp encoder is not installed", encodeCommand);

		var (inputPath, outputPath) = CreateTemporaryFiles();
		try
		{
			await using (var inputPathWriter = File.OpenWrite(inputPath))
			{
				await input.CopyToAsync(inputPathWriter);
			}

			var encodeArguments = new[] { inputPath, "-o", outputPath, "-preset", "photo" };
			var encodeStandardErrorPipe = new StringBuilder();

			await Cli.Wrap(encodeCommand)
				.WithArguments(encodeArguments)
				.WithStandardErrorPipe(PipeTarget.ToStringBuilder(encodeStandardErrorPipe))
				.ExecuteAsync();

			var encodedContent = await File.ReadAllBytesAsync(outputPath);
			return encodedContent;
		}
		catch
		{
			return null;
		}
		finally
		{
			DeleteTemporaryFiles(inputPath, outputPath);
		}
	}

	private static (string inputPath, string outputPath) CreateTemporaryFiles()
	{
		var temporaryInput = Path.GetTempFileName();
		var temporaryOutput = Path.GetTempFileName();
		return (temporaryInput, temporaryOutput);
	}

	private static void DeleteTemporaryFiles(string inputPath, string outputPath)
	{
		if (File.Exists(inputPath)) File.Delete(inputPath);
		if (File.Exists(outputPath)) File.Delete(outputPath);
	}

	private string GetEncodeCommand()
	{
		return (_os, _architecture) switch
		{
			("linux", Architecture.Arm64) => Path.Combine("Assets", "libwebp", "linux_arm64", "binaries", "cwebp"),
			("linux", Architecture.X64) => Path.Combine("Assets", "libwebp", "linux_x64", "binaries", "cwebp"),
			("linux", Architecture.X86) => Path.Combine("Assets", "libwebp", "linux_x64", "binaries", "cwebp"),
			("windows", Architecture.X64) => Path.Combine("Assets", "libwebp", "windows_x64", "binaries", "cwebp.exe"),
			_ => throw new ApplicationException("Unsupported platform")
		};
	}
}