export type CursorResponse<T> = {
    content: T[];
    hasNext: boolean;
    hasPrev: boolean;
    page: number;
}

export type CoffeeDto = {
    id: string;
    title: string;
    price: number;
}