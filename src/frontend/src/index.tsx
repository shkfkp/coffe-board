import React from 'react';
import {createRoot} from 'react-dom/client';

import Router from "./components/router";

import "./index.css";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {BrowserRouter} from "react-router-dom";

const queryClient = new QueryClient();
const rootElement = document.getElementById("root");
const root = createRoot(rootElement!);

root.render(
    <React.StrictMode>
        <BrowserRouter>
            <QueryClientProvider client={queryClient}>
                <div className={'min-h-screen bg-background font-sans antialiased'}>
                    <Router/>
                </div>
            </QueryClientProvider>
        </BrowserRouter>
    </React.StrictMode>
);
//
// ReactDOM.createRoot(document.getElementById('root')!).render(
//     <React.StrictMode>
//         <BrowserRouter>
//             <QueryClientProvider client={queryClient}>
//                 <div className={'min-h-screen bg-background font-sans antialiased'}>
//                     <Router/>
//                 </div>
//             </QueryClientProvider>
//         </BrowserRouter>
//     </React.StrictMode>
// );