import { Fragment, FunctionComponent, useCallback, useState } from 'react';
import { useInfiniteQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import wretch from "wretch";
import { CoffeeDto, CursorResponse } from "../../types/api";

interface AdminProps {
}

interface AddCoffeeFormProps {
    onSubmit: (data: FormData) => void | Promise<void>
}

const AddCoffeeForm: FunctionComponent<AddCoffeeFormProps> = ({ onSubmit }) => {
    const [file, setFile] = useState<File | null>(null);
    const [title, setTitle] = useState<string>('');
    const [price, setPrice] = useState<number>(1);

    const handleSubmit = useCallback(() => {
        if (file === null) {
            alert('Please select a file');
            return;
        }

        if (title.length < 3) {
            alert('Please specify a title with at least 3 characters')
            return;
        }

        const formData = new FormData();
        formData.append("image", file, file.name);
        formData.append("title", title);
        formData.append("price", price.toFixed(2));

        onSubmit(formData);

    }, [file, title, price, onSubmit]);

    return (
        <div>
            <div className='flex items-center space-x-2'>
                <label>Image</label>
                <input className='flex-grow' type='file'
                    onChange={(e) => setFile(e.target.files ? e.target.files[0] : null)} />
            </div>
            <div className='flex items-center space-x-2'>
                <label>Title</label>
                <input className='flex-grow' value={title} onChange={(e) => setTitle(e.target.value ?? '')} />
            </div>
            <div className='flex items-center space-x-2'>
                <label>Price</label>
                <input className='flex-grow' type='number' min={1} max={500} step={1} value={price}
                    onChange={(e) => setPrice(Number.isNaN(e.target.valueAsNumber) ? 1 : e.target.valueAsNumber)} />
            </div>
            <button className='bg-stone-200 shadow px-4 px-2 rounded-md' onClick={handleSubmit}>
                Submit
            </button>
        </div>
    )

}

const Admin: FunctionComponent<AdminProps> = ({ }) => {
    const qc = useQueryClient();

    const query = useInfiniteQuery({
        queryKey: ['coffee', 'billboard'],
        queryFn: ({ pageParam }) =>
            wretch
                .default()
                .url(`${process.env.api ?? ''}/api/Billboard/cursor?cursor=${pageParam}`)
                .get()
                .json<CursorResponse<CoffeeDto>>(),
        initialPageParam: 1,
        getPreviousPageParam: (firstPage) => firstPage.hasPrev ? firstPage.page - 1 : undefined,
        getNextPageParam: (lastPage) => lastPage.hasNext ? lastPage.page + 1 : undefined
    });

    const removeMutation = useMutation({
        mutationFn: (id: string) =>
            wretch
                .default()
                .url(`${process.env.api ?? ''}/api/Billboard/remove/${id}`)
                .delete()
                .json(),
        onSuccess: () => qc.invalidateQueries({ queryKey: ['coffee', 'billboard'] })
    });

    const addMutation = useMutation({
        mutationFn: (data: FormData) => wretch
            .default()
            .url(`${process.env.api ?? ''}/api/Billboard/add`)
            .body(data)
            .post()
            .json(),
        onSuccess: () => qc.invalidateQueries({ queryKey: ['coffee', 'billboard'] }),
        onError: () => alert('Error submitting your coffee :(')
    });

    const resetMutation = useMutation({
        mutationFn: () => wretch
            .default()
            .url(`${process.env.api ?? ''}/api/Billboard/reset`)
            .post()
            .json(),
        onSuccess: () => qc.invalidateQueries({ queryKey: ['coffee', 'billboard'] })
    });

    return (
        <div className='flex flex-row items-start space-x-2'>
            <div className='flex flex-col space-y-4'>
                <div className='flex items-center space-x-2'>
                    <button className='bg-stone-200 shadow px-4 px-2 rounded-md'
                        onClick={() => resetMutation.mutate()}>Reset to seed data
                    </button>
                </div>
                <AddCoffeeForm onSubmit={addMutation.mutate} />
            </div>

            <div className='flex-grow'>
                {query.data?.pages.map(page => (
                    <Fragment key={page.page}>
                        {page.content.map(coffee => (
                            <div key={coffee.id} className='flex w-[500px] h-24 space-x-4 items-center justify-between'>
                                <img className='aspect-square h-20'
                                    src={`${process.env.api ?? ''}/api/Assets/image/${coffee.id}`} />
                                <p className='font-semibold'>{coffee.title}</p>
                                <p className='font-bold'>{coffee.price.toFixed(2)}€</p>
                                <button onClick={() => removeMutation.mutate(coffee.id)}>Remove</button>
                            </div>
                        ))}
                    </Fragment>
                ))}
                {
                    query.hasNextPage && (
                        <div className='col-span-1 h-24 rounded-xl border bg-white shadow'>
                            <button className='h-full w-full' onClick={() => query.fetchNextPage()}>
                                Load More
                            </button>
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default Admin;