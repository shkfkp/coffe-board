import { Fragment, FunctionComponent } from 'react';
import { useInfiniteQuery } from "@tanstack/react-query";
import wretch from "wretch";
import { CoffeeDto, CursorResponse } from "../../types/api";
import CoffeeCard from "../ui/coffee-card";

interface HomeProps {
}

const Home: FunctionComponent<HomeProps> = ({ }) => {
    const query = useInfiniteQuery({
        queryKey: ['coffee', 'billboard'],
        queryFn: ({ pageParam }) =>
            wretch
                .default()
                .url(`${process.env.api ?? ''}/api/Billboard/cursor?cursor=${pageParam}`)
                .get()
                .json<CursorResponse<CoffeeDto>>(),
        initialPageParam: 1,
        getPreviousPageParam: (firstPage) => firstPage.hasPrev ? firstPage.page - 1 : undefined,
        getNextPageParam: (lastPage) => lastPage.hasNext ? lastPage.page + 1 : undefined
    })

    return (
        <div className={'grid grid-cols-4 gap-4'}>
            {query.data?.pages.map(page => (
                <Fragment key={page.page}>
                    {page.content.map(coffee => (
                        <CoffeeCard key={coffee.id} coffee={coffee} />
                    ))}
                </Fragment>
            ))}
            {
                query.hasNextPage && (
                    <div className='col-span-1 h-96 rounded-xl border bg-white shadow'>
                        <button className='h-full w-full' onClick={() => query.fetchNextPage()}>
                            Load More
                        </button>
                    </div>
                )
            }

        </div>
    );
};

export default Home;