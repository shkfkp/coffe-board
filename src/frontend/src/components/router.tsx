import React, {FunctionComponent} from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import Layout from "./ui/layout";
import Home from "./pages/home";
import Admin from "./pages/admin";

interface RouterProps {
}

const Router: FunctionComponent<RouterProps> = ({}) => {
    return (
        <Routes>
            <Route path={'/'} element={<Layout/>}>
                <Route index element={<Home/>}/>
                <Route path={'admin'} element={<Admin/>}/>
                <Route path={'*'} element={<Navigate to={'/'} replace={true}/>}/>
            </Route>
        </Routes>
    );
};

export default Router;