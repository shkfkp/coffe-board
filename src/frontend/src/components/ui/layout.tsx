import {FunctionComponent} from 'react';
import {Outlet} from "react-router-dom";
import Header from "./header";

interface LayoutProps {
}

const Layout: FunctionComponent<LayoutProps> = ({}) => {
    return (
        <div className='relative flex min-h-screen flex-col'>
            <Header/>
            <main className='flex flex-grow min-w-80'>
                <div className='flex flex-1 justify-center p-4 bg-neutral-50'>
                    <div className='max-w-screen-xl'>
                        <Outlet/>
                    </div>
                </div>
            </main>
        </div>
    );
};

export default Layout;