import {FunctionComponent} from 'react';
import {CoffeeDto} from "../../types/api";

interface CoffeeCardProps {
    coffee: CoffeeDto
}

const CoffeeCard: FunctionComponent<CoffeeCardProps> = ({coffee}) => {
    return (
        <div className='col-span-1 h-96 rounded-xl border bg-white shadow'>
            <img className='aspect-square rounded-t-xl' src={`${process.env.api ?? ''}/api/Assets/image/${coffee.id}`}/>
            <div className='flex justify-between py-2 px-4 text-sm'>
                <p className='font-semibold'>{coffee.title}</p>
                <p className='font-bold'>{coffee.price.toFixed(2)}€</p>
            </div>
        </div>
    );
};

export default CoffeeCard;