import {FunctionComponent} from 'react';
import {Link, useLocation} from "react-router-dom";

interface HeaderProps {
}

const Header: FunctionComponent<HeaderProps> = ({}) => {
    const {pathname} = useLocation();
    return (
        <header
            className='sticky top-0 z-40 w-full border-b bg-neutral-100'>
            <div className='flex h-14 items-center justify-center space-x-4'>
                <Link
                    to={'/'}
                    className={pathname === '/' ? 'text-neutral-800 border-b' : 'text-neutral-800/70'}
                >
                    Home
                </Link>
                <Link
                    to={'/admin'}
                    className={pathname === '/admin' ? 'text-neutral-800 border-b' : 'text-neutral-800/70'}
                >
                    Admin
                </Link>
            </div>
        </header>
    );
};

export default Header;